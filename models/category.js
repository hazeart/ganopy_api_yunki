var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var db = mongoose.connect('mongodb://localhost/ganopy');
autoIncrement.initialize(db);

var categorySchema = new Schema({

	category_id : { type: Number},
	name : { type : String},
	image : { type : String },
	description : { type : String }

});

categorySchema.plugin(autoIncrement.plugin, {
    model: 'category',
    field: 'category_id',
    startAt: 0,
    incrementBy: 1
});

module.exports = mongoose.model('category', categorySchema);
