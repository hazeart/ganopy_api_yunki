var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var autoIncrement = require('mongoose-auto-increment');
var db = mongoose.connect('mongodb://localhost/ganopy');
autoIncrement.initialize(db);

var productSchema = new Schema({

	product_id : { type : Number },
	title : { type : String, required : [true, '제목을 입력해 주세요.'] },
	price : {
		origin : { type : Number },
		sale : { type : Number }
	},
	cover : { type : String },
	created : { type : String, default: Date.now },
	edited : { type : String, default: Date.now },
	description : { type : String, required : [true, '내용을 입력해 주세요.'] },
	link : { type : String },
	author : { type : Schema.Types.ObjectId, ref : 'User' },
	category : { type : Schema.Types.ObjectId, ref : 'Category' },
	brand : { type : Schema.Types.ObjectId, ref : 'Brand' },
	comment : [{ type : Schema.Types.ObjectId, ref : 'Comment' }],
	likes : [{ type : Schema.Types.ObjectId, ref : 'User' }],

});

productSchema.plugin(autoIncrement.plugin, {
    model: 'product',
    field: 'product_id',
    startAt: 0,
    incrementBy: 1
});

module.exports = mongoose.model('product', productSchema);
