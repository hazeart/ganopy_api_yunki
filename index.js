var restify = require('restify');
var mongoose = require('mongoose');
var fs = require('fs');

var server = restify.createServer();
server.use(restify.bodyParser({
	mapParams: false,
	multipartFileHandler: function(part) {
        part.on('data', function(data) {
        	console.log(part.filename);
			fs.writeFile('./uploads/' + part.filename, data, function(err, data) {
				if(err) throw err;
				console.log(data);
			});
        });
    },
}));
server.use(restify.queryParser());

// product CRUD
var Product = require('./models/product.js');

var productHandler = {};
productHandler.post = function (req, res, next) {
	var data = req.body;
	var newProduct = new Product(data);
	newProduct.save(function (err) {
		if (err) {
			res.send(200, err.errors);
		} else {
			res.send(200, 'success');
		}
	});
}

productHandler.get = function (req, res, next) {
	console.log(req.params);
	var product_id = req.params.product_id;
	// res.send(200, product_id);
	Product.findOne(
		{'product_id':product_id},function (err,product) {
			res.send(200, product);
		}
	);
}

productHandler.getlist = function (req, res, next) {
	console.log(req.query);
	var result = Product.find(
		{},function (err,products) {
			res.send(200, products);
		}
	);
}

productHandler.put = function (req, res, next) {
	console.log(req.params);
	var product_id = req.params.product_id;
	res.send(200, product_id);
}

productHandler.del = function (req, res, next) {
	console.log(req.params);
	var product_id = req.params.product_id;
	res.send(200, product_id);
}

server.post('/product', productHandler.post);
server.get('/products', productHandler.getlist);
server.get('/product/:product_id', productHandler.get);
server.put('/product/:product_id', productHandler.put);
server.del('/product/:product_id', productHandler.del);


server.listen(8080, function() {
	console.log('%s listening at %s', server.name, server.url);
});
