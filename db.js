var mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
var db = mongoose.connect('mongodb://localhost/ganopy');
autoIncrement.initialize(db);

module.exports = function () {
	return {
		mongoose.connect('mongodb://localhost/ganopy');
	}
}
